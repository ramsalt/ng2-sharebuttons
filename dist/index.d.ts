import { ShareButtonsComponent } from './components/share-buttons/share-buttons.component';
import { ShareButtonComponent } from './components/share-button/share-button.component';
import { ShareButtonsService } from "./service/share-buttons.service";
import { NFormatterPipe } from './helpers/n-formatter.pipe';
import { ShareProvider } from './helpers/share-provider.enum';
import { ShareButton, ShareArgs } from './helpers/share-buttons.class';
export declare class ShareButtonsModule {
}
export { ShareButtonsComponent, ShareButtonComponent, ShareProvider, ShareButton, NFormatterPipe, ShareButtonsService, ShareArgs };
