"use strict";var ShareButton=function(){function t(t,r,s){this.provider=t,this.template=r,this.classes=s}return t}();exports.ShareButton=ShareButton;var ShareArgs=function(){function t(t,r,s,e,i){this.url=t,this.title=r,this.description=s,this.image=e,this.tags=i}return t}();exports.ShareArgs=ShareArgs;
//# sourceMappingURL=share-buttons.class.js.map
