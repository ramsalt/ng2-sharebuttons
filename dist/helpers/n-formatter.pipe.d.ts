import { PipeTransform } from '@angular/core';
export declare class NFormatterPipe implements PipeTransform {
    transform(value: any, args?: any): any;
    nFormatter(num: any, digits: any): any;
}
