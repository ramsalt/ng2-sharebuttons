export interface ShareButtonsInterface {
    share(type: any, url: any): any;
    count(type: any, url: any): any;
    windowAttr(): any;
}
